let request = require('request');

let baseURL = process.env.baseURL || 'http://localhost/api/';

/**
 * Produce a random number between and the low and high (exclusive)
 * @param low {number}
 * @param high {number}
 * @return {number}
 */
function randomInt(low, high) {
    return Math.floor(Math.random() * (high - low) + low)
}

/**
 * Sets a random state to the cube
 */
function setRandomPattern() {
    // Get the available patterns
    request.get({url:`${baseURL}patterns`, json:true}, function (error, response, body) {
        let patternName = body[randomInt(0, body.length)].name;

        // Determine the steed
        let speed = randomInt(5, 20);
        if(randomInt(0,2))
            speed = speed * -1;

        let next = randomInt(1000, 50000);

        console.log(`${new Date().toString()} - Setting cube to ${patternName} with speed ${speed}. Next one in ${Math.round(next/1000)} seconds`);

        // Set the pattern
        request({
                method: 'POST',
                uri: `${baseURL}pattern`,
                json: {
                    "name": patternName,
                    "speed": speed,
                },
            },
            function (error) {
                if (error) {
                    return console.error('upload failed:', error);
                }
            });

        // Queue the next pattern
        setTimeout(setRandomPattern, next);
    })
}

// Trigger the first random pattern. The others will be triggered by the setRandomState function
setRandomPattern();

