FROM node:lts

RUN curl -sSL https://gitlab.com/madled/led-shuffle/-/archive/master/led-shuffle-master.tar.gz \
		| tar -v -C / -xz
RUN mv /led-shuffle-master /shuffle
WORKDIR /shuffle
RUN npm install

WORKDIR /shuffle
CMD ["node", "./main.js"]